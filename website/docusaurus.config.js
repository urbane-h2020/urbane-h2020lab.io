/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'URBANE',
  tagline: 'Upscaling Innovative Green Urban Logistics Solutions Through Multi-Actor Collaboration and PI-inspired Last Mile Deliveries',
  url: 'https://urbane-h2020.gitlab.io',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/cropped-favicon-urbane-32x32.png',
  organizationName: 'GitLab', // Usually your GitHub org/user name.
  projectName: 'URBANE', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'URBANE',
      logo: {
        alt: 'My Site Logo',
        src: 'img/logo.svg',
      },
      items: [
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Docs',
          position: 'left',
        },
        {to: 'blog', label: 'Blog', position: 'left'},
        {
          href: 'https://gitlab.com/pages/docusaurus',
          label: 'GitLab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Getting Started',
              to: 'docs/',
            },
          ],
        },
        {
          title: 'Community',
          items: [
            {
              label: 'LinkedIn',
              href: 'https://www.linkedin.com/company/urbane-project/',
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              label: 'Blog',
              to: 'blog',
            },
            {
              label: 'Gitlab',
              href: 'https://gitlab.com/urbane-h2020',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} URBANE Horizon 2020. Built with Docusaurus.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/blog/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
